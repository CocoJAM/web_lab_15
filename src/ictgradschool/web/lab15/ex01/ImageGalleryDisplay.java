package ictgradschool.web.lab15.ex01;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.Document;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by ljam763 on 10/05/2017.
 */
public class ImageGalleryDisplay  extends HttpServlet {
//   private ServletContext servletContext = getServletContext();
//   private String fullPhotoPath = servletContext.getRealPath("/Photos");

    public ImageGalleryDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String dir = System.getProperty("user.dir");
        System.out.println(dir);
        PrintWriter out = response.getWriter();
        out.println("<html>\n<head><title>Server response</title>");
        out.println("</head>\n<body>");
        String direction = "C:\\Users\\ljam763\\IdeaProjects\\web_lab_15\\out\\artifacts\\web_lab_15_war_exploded\\WEB-INF\\Photos";
        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");
        System.out.println(fullPhotoPath);
        File file = new File(direction);
        File[] ListOfFiles = file.listFiles();
        for (File listOfFile : ListOfFiles) {
            String photo = fullPhotoPath+"\\"+listOfFile.getName();
            if (photo.endsWith("_thumbnail.png")){
                out.print("<a href=\"\\Photos\\" + listOfFile.getName().replace("_thumbnail.png", ".jpg") + "\">");
            out.print("<img src=\"\\Photos\\" + listOfFile.getName() + "\">");
            out.print("</a>");
            out.print("<p> Name: "+ listOfFile.getName().replace("_", " ").replace("thumbnail.png","") + ", Size: "+ listOfFile.length()+"</p>");
            }
        }
        out.println("</body></html>");
    }
}
